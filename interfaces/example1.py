import abc 

class AbstractShape(abc.ABC):
    @abc.abstractclassmethod
    def draw():
        pass 


class Square(AbstractShape):
    pass 


square = Square()

"""
This will raise an error as the Square class must implement
the draw() method
"""
