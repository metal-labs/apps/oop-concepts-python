import abc 

class AbstractShape(abc.ABC):
    @abc.abstractclassmethod
    def draw():
        pass 


class Square(AbstractShape):
    def draw(self):
        print('drawing square')


square = Square()
square.draw()

"""
The Square class is derived from the AbstractShape class.
It must implment the draw() method
"""
