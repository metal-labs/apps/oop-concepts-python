import abc
from typing import Optional


class AbstractCreditCard(abc.ABC):
    @property
    @abc.abstractproperty
    def card_number(self):
        pass

    @property
    @abc.abstractproperty
    def card_expiry(self):
        pass

    @property
    @abc.abstractproperty
    def card_sec_code(self):
        pass

    @abc.abstractclassmethod
    def pay():
        pass

    @abc.abstractclassmethod
    def decline():
        pass


class Visa(AbstractCreditCard):
    card_number = ""
    card_expiry = ""
    card_sec_code = ""

    def __init__(
        self,
        card_number: Optional[str] = None,
        card_expiry: Optional[str] = None,
        card_sec_code: Optional[str] = None,
    ) -> None:
        super().__init__()
        self.card_number = card_number
        self.card_expiry = card_expiry
        self.card_sec_code = card_sec_code

    def pay(self):
        print("make visa payment")

    def decline(self):
        print("visa card declined")


class Amex(AbstractCreditCard):
    card_number = ""
    card_expiry = ""
    card_sec_code = ""

    def __init__(
        self,
        card_number: Optional[str] = None,
        card_expiry: Optional[str] = None,
        card_sec_code: Optional[str] = None,
    ) -> None:
        super().__init__()
        self.card_number = card_number
        self.card_expiry = card_expiry
        self.card_sec_code = card_sec_code

    def pay(self):
        print("make amex payment")

    def decline(self):
        print("amex card declined")


visa = Visa()
visa.pay()

amex = Amex()
amex.pay()
amex.decline()
