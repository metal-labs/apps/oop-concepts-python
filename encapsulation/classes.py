class Person:
    def __init__(self, name: str) -> None:
        self.name = name # public member

    def info(self):
        print('Name: ', self.name)
        

class Company:
    def __init__(self) -> None:
        self._project = "chat gpt 4" # protected member

class Employee(Person, Company):
    def __init__(self, name: str, role: str, salary: float) -> None:
        super().__init__(name)
        self.role = role # public member
        self.__salary = salary # private member
        Company.__init__(self)
   

    def info(self):
        print('Name: ', self.name, 'Salary: ', self.__salary)

    def project(self):
        print('Project: ', self._project)

    def get_salary(self):
        return self.__salary
    
    def set_salary(self, salary: float):
        self.__salary = salary
