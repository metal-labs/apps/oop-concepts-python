from classes import Employee

emp = Employee('Ben', 'acrobrat', 500000.99)

print(emp.get_salary())

emp.set_salary(250000.89)

print(emp.get_salary())
