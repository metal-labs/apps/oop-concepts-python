from classes import Employee

emp = Employee('Ben', 'software engineer', 20.00)

emp.info()

print('Name: ', emp.name) 
print('Name: ', emp.name, 'Project: ', emp._project)
print('Name: ', emp.name, 'Project: ', emp._project, 'Salary: ', emp._Employee__salary) # name mangling can access private member 
print('Name: ', emp.name, 'Project: ', emp._project, 'Salary: ', emp.__salary) # raises AttributeError as salary is private